set nocompatible
syntax enable
syntax on
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
behave mswin
set tags=tags
"for ps_color theme of warm -----2 lines below ---|
color blackboard
set showtabline=2
set encoding=utf-8
set termencoding=prc
set langmenu=zh_CN.UTF-8
language message zh_CN.UTF-8
let JavaBrowser_Ctags_Cmd = 'C:\windows\system32\ctags'
let Tlist_Ctags_Cmd = "C:\windows\system32\ctags"
"learning map
let mapleader = "-"
nnoremap <leader>ev :vsplit $MYVIMRC<cr>
"nnoremap <leader>sv :source $MYVIMRC<cr>
"end of learing map
"userful nnoremapping
"surrounding current word with quote
nnoremap <leader>" viw<esc>a"<esc>hbi"<esc>lel
"quickly exit insert mode
inoremap jk <esc>
"insert iabbrev here
" Shortcut to rapidly toggle `set list`
nmap <leader>l :set list!<CR>
 
" Use the same symbols as TextMate for tabstops and EOLs

source $VIMRUNTIME/delmenu.vim
source $VIMRUNTIME/menu.vim
set fileencoding=utf-8
set fileencodings=utf-8,ucs-bom,gbk,prc,cp936
set guioptions+=b
set autoindent
set wrap
set tabstop=4
set tags=D:\Dev_Works\V7\web\java\code\bss\tags,D:\Dev_Works\V611\code\bss\tags,D:\Dev_Works\V613\java\code\tags
set shiftwidth=4
set number
map <F2> <Esc>:tabnext<CR>
nmap <F11> :JavaBrowser<CR>
imap <F11> <ESC><F11>
command -nargs=1 SetAlpha call libcallnr("vimtweak.dll", "SetAlpha", <args>)
command -nargs=0 TopMost call libcallnr("vimtweak.dll", "EnableTopMost", 1)
command -nargs=0 NoTopMost call libcallnr("vimtweak.dll", "EnableTopMost", 0)
autocmd FileType html set omnifunc=htmlcomplete#CompleteTags
autocmd FileType c set omnifunc=ccomplete#Complete
autocmd FileType xml set omnifunc=xmlcomplete#CompleteTags
autocmd FileType css set omnifunc=csscomplete#CompleteCSS
autocmd FileType java JavaBrowser
autocmd FileType javascript set omnifunc=javascriptcomplete#CompleteJS
autocmd FileType python set omnifunc=pythoncomplete#Complete
autocmd FileType python setlocal et sta sw=4 sts=4
autocmd FileType php set omnifunc=phpcomplete#CompletePHP
autocmd BufReadPost * if line("'\"") && line("'\"") <= line("$") | exe "normal`\"" | endif
au BufNewFile,BufRead *.prc set filetype=sql
set nocp
filetype plugin indent on
"filetype plugin on
if version >= 603
    set helplang=cn
endif
set guifont=Courier_New:h10
set gfw=NSimSun:h10
set diffexpr=MyDiff()
function MyDiff()
  let opt = '-a --binary '
  if &diffopt =~ 'icase' | let opt = opt . '-i ' | endif
  if &diffopt =~ 'iwhite' | let opt = opt . '-b ' | endif
  let arg1 = v:fname_in
  if arg1 =~ ' ' | let arg1 = '"' . arg1 . '"' | endif
  let arg2 = v:fname_new
  if arg2 =~ ' ' | let arg2 = '"' . arg2 . '"' | endif
  let arg3 = v:fname_out
  if arg3 =~ ' ' | let arg3 = '"' . arg3 . '"' | endif
  let eq = ''
  if $VIMRUNTIME =~ ' '
    if &sh =~ '\<cmd'
      let cmd = '""' . $VIMRUNTIME . '\diff"'
      let eq = '"'
    else
      let cmd = substitute($VIMRUNTIME, ' ', '" ', '') . '\diff"'
    endif
else
    let cmd = $VIMRUNTIME . '\diff'
  endif
  silent execute '!' . cmd . ' ' . opt . arg1 . ' ' . arg2 . ' > ' . arg3 . eq
endfunction

" python auto-complete code
" Typing the following (in insert mode):
"   os.lis<Ctrl-n>
" will expand to:
"   os.listdir(
" Python 自动补全功能，用 Ctrl-N 调用
"if has("autocmd")
"  autocmd FileType python set complete+=k'C:\Program Files\Vim\pydiction' isk+=.,
"endif
